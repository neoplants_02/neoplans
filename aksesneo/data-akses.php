<!DOCTYPE html>
<html>
    <head>
        <?php include "header.php"; ?>
        <title>Data Akses</title>
    </head>
    <body>
        <?php include "menu.php"; ?>

        <!-- isi -->

        <div class="container-fluid">
        <h3> 
            Data Akses
        </h3>
        <table class="table table-bordered">
            <thead>
                <tr style="background-color: gray; color: white;">
                    <th style="width: 10px; text-align: center">NO</th>
                    <th style="width: 100px; text-align: center">NO KARTU</th>
                    <th style="width: 200px; text-align: center">NAMA</th>
                    <th style="width: 300px; text-align: center">ALAMAT</th>
                    <th style="width: 300px; text-align: center">LINK AKSES</th>
                    <th style="width: 300px; text-align: center">NO HP</th>
                    <th style="width: 50px; text-align: center">KET</th>
                </tr>
            </thead>
        <tbody>

        <?php
        // koneksi ke database
        include "koneksi.php";

        // baca data akses
        $sql = mysqli_query($koneksi, "SELECT * FROM akses");
        $no = 0;
        while($data = mysqli_fetch_array($sql)) {
            $no++;
        ?>

            <tr>
                <td style="text-align: center"><?php echo $no; ?></td>
                <td style="text-align: center"><?php echo $data['nokartu']; ?></td>
                <td style="text-align: center"><?php echo $data['nama']; ?></td>
                <td style="text-align: center"><?php echo $data['alamat']; ?></td>
                <td style="text-align: center"><?php echo $data['link_akses']; ?></td>
                <td style="text-align: center"><?php echo $data['nohp']; ?></td>
                <td style="text-align: center">
                    <a href="edit.php?id=<?php echo $data['id']; ?>">Edit</a> |
                    <a href="hapus.php?id=<?php echo $data['id']; ?>">Hapus</a>
                </td>
            </tr>
            <?php } ?>
        </tbody>
        </table>

        <!-- tombol tambah-->

        <a href="tambah.php">
            <button class="btn btn-primary">Tambah Data Akses</button>
        </a>
        
        </div>
        
        <?php include "footer.php"; ?>
    </body>
</html>