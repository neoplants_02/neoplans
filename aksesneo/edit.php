<!-- proses penyimpanan -->
<?php
    include "koneksi.php";

    // baca id data yang akan diedit
    $id = $_GET['id'];

    // baca data akses berdasarkan id
    $cari = mysqli_query($koneksi, "SELECT * FROM akses WHERE id='$id'");
    $hasil = mysqli_fetch_array($cari);


    // jika tombol simpan di klik
    if(isset($_POST['btnSimpan'])){

        // baca isi inputan form
    $nokartu = $_POST['nokartu'];
    $nama = $_POST['nama'];
    $alamat = $_POST['alamat'];
    $link_akses = $_POST['link_akses'];
    $nohp = $_POST['nohp'];

    // simpan ke tabel akses
    $simpan = mysqli_query($koneksi, "update akses set nokartu='$nokartu', nama='$nama', alamat='$alamat', link_akses='$link_akses'
    , nohp='$nohp' WHERE id='$id' ");
   
    // jika berhasil tersimpan, tampilan pesan tersimpan

    // kembali ke data akses
    if($simpan) {
        echo "<script>
                alert('Tersimpan');
                location.replace('data-akses.php');
                </script>
                ";
    } else {
        echo "<script>
                alert('Gagal Tersimpan');
                location.replace('data-akses.php');
                </script>
                ";
    }

    }

?>


<!DOCTYPE html>
<html>
    <head>
        <?php include "header.php"; ?>
        <title>Tambah Data Akses</title>
    </head>
    <body>
        <?php include "menu.php"; ?>

        <!-- isi -->

        <div class="container-fluid">
        <h3> 
            Tambah Data Akses
        </h3>

        <!-- form input -->
        <form method="POST">
            <div class="form-group">
                <label>No Kartu</label>
                <input type="text" name="nokartu" id="nokartu" placeholder="No Kartu RFID"
                class="form-control" style="width: 200px" value="<?php echo $hasil['nokartu']; ?>">
            </div>

            <div class="form-group">
                <label>Nama</label>
                <input type="text" name="nama" id="nama" placeholder="Nama"
                class="form-control" style="width: 200px" value="<?php echo $hasil['nama']; ?>">
            </div>

            <div class="form-group">
                <label>Alamat</label>
                <textarea type="text" name="alamat" id="alamat" placeholder="Alamat"
                class="form-control" style="width: 200px"><?php echo $hasil['alamat']; ?></textarea>
            </div>

            <div class="form-group">
                <label>Link Akses</label>
                <textarea type="text" name="link_akses" id="link_akses" placeholder="Link Akses"
                class="form-control" style="width: 200px"><?php echo $hasil['link_akses']; ?></textarea>
                <img src="images/barcode.jpeg" alt="gambarbarcode" style="width:100px;height:120px"><br>
            </div>

            <div class="form-group">
                <label>No Hp</label>
                <textarea type="text" name="nohp" id="nohp" placeholder="No Hp"
                class="form-control" style="width: 200px"><?php echo $hasil['nohp']; ?></textarea>
            </div>

            <button class="btn btn-primary" name="btnSimpan" id="btnSimpan"> 
                Simpan
            </button>

        </form>
        </div>
        
        <?php include "footer.php"; ?>
    </body>
</html>