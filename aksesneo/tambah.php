<!-- proses penyimpanan -->
<?php
    include "koneksi.php";

    // jika tombol simpan di klik
    if(isset($_POST['btnSimpan'])){

        // baca isi inputan form
    $nokartu = $_POST['nokartu'];
    $nama = $_POST['nama'];
    $alamat = $_POST['alamat'];
    $link_akses = $_POST['link_akses'];
    $nohp = $_POST['nohp'];

    // simpan ke tabel akses
    $simpan = mysqli_query($koneksi, "INSERT INTO akses(nokartu, nama, alamat, link_akses, nohp)VALUES('$nokartu', '$nama',
    '$alamat', '$link_akses', '$nohp')");
    // jika berhasil tersimpan, tampilan pesan tersimpan

    // kembali ke data akses
    if($simpan) {
        echo "<script>
                alert('Tersimpan');
                location.replace('data-akses.php');
                </script>
                ";
    } else {
        echo "<script>
                alert('Gagal Tersimpan');
                location.replace('data-akses.php');
                </script>
                ";
    }

    }
    // kosongkan tabel tmprfid
    mysqli_query($koneksi, "DELETE FROM tmprfid");

?>


<!DOCTYPE html>
<html>
    <head>
        <?php include "header.php"; ?>
        <title>Tambah Data Akses</title>

        <!-- pembacaan no kartu otmatis dengan js -->
        <script type="text/javascript">
            $(document).ready(function(){
                setInterval(function(){
                    $("#norfid").load('nokartu.php')
                }, 0); // pembacaan file no kartu sesuai detik karena 0 jadi refresh langsung muncul
            });
        </script>
    </head>

    <body>
        <?php include "menu.php"; ?>

        <!-- isi -->

        <div class="container-fluid">
        <h3> 
            Tambah Data Akses
        </h3>

        <!-- form input -->
        <form method="POST">
            <div id="norfid">
            </div>
            
            <div class="form-group">
                <label>Nama</label>
                <input type="text" name="nama" id="nama" placeholder="Nama"
                class="form-control" style="width: 200px">
            </div>

            <div class="form-group">
                <label>Alamat</label>
                <textarea type="text" name="alamat" id="alamat" placeholder="Alamat"
                class="form-control" style="width: 200px"></textarea>
            </div>

            <div class="form-group">
                <label>Link Akses</label>
                <textarea type="text" name="link_akses" id="link_akses" placeholder="Link Akses"
                class="form-control" style="width: 200px"></textarea>
                <img src="images/barcode.jpeg" alt="gambarbarcode" style="width:100px;height:120px"><br>
            </div>

            <div class="form-group">
                <label>No Hp</label>
                <input type="text" name="nohp" id="nohp" placeholder="No Hp"
                class="form-control" style="width: 200px">
            </div>

            <button class="btn btn-primary" name="btnSimpan" id="btnSimpan"> 
                Simpan
            </button>

        </form>
        </div>
        
        <?php include "footer.php"; ?>
    </body>
</html>